# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
import configuration
import shareholder

def register():
    Pool.register(
        configuration.ShareholdingConfiguration,
        configuration.ShareholdingEventType,
        configuration.ShareholdingType,
        configuration.ShareholdingPackage,
        configuration.ShareholdingPackageLines,
        shareholder.CompanyShareholder,
        shareholder.CompanyShareholdings,
        shareholder.ShareholdingsEvents,
        shareholder.CreatePackageStart,
        module='company_shareholding', type_='model')
    Pool.register(
        shareholder.CompanyShareholdingsReport,
        module='company_shareholding', type_='report')
    Pool.register(
        shareholder.CreatePackage,
        module='company_shareholding', type_='wizard')
