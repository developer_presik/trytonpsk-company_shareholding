from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval


__all__ = ['ShareholdingConfiguration', 'ShareholdingEventType',
           'ShareholdingType', 'ShareholdingPackage',
           'ShareholdingTypePackage']


class ShareholdingConfiguration(ModelSQL, ModelView):
    'Configuration'
    __name__ = 'company.shareholding.configuration'

    shareholding_sequence = fields.Many2One('ir.sequence',
        'Shareholding Sequence', domain=[
            ('company', '=', Eval('company')),
            ('code', '=', 'company.shareholder'),
        ], required=True)
    shareholder_sequence = fields.Many2One('ir.sequence',
        'Shareholder Sequence', domain=[
            ('company', '=', Eval('company')),
            ('code', '=', 'company.shareholder'),
        ], required=True)
    company = fields.Many2One('company.company', 'Company', required=True)


class ShareholdingEventType(ModelSQL, ModelView):
    'Shareholding Type Event'
    __name__ = 'company.shareholding.event_type'
    _rec_name = 'name'
    name = fields.Char('Name', required=True)

    @classmethod
    def __setup__(cls):
        super(ShareholdingEventType, cls).__setup__()


class ShareholdingType(ModelSQL, ModelView):
    'Shareholding Type'
    __name__ = 'company.shareholding_type'
    _rec_name = 'name'
    name = fields.Char('name', required=True)
    value = fields.Numeric('Value')
    description = fields.Char('Description')

    @classmethod
    def __setup__(cls):
        super(ShareholdingType, cls).__setup__()


class ShareholdingPackage(ModelSQL, ModelView):
    'Shareholding Package'
    __name__ = 'company.shareholding.package'
    name = fields.Char('Name', required=True)
    lines = fields.One2Many('company.shareholding.lines', 'package',
                            'Lines', required=True)


class ShareholdingPackageLines(ModelSQL, ModelView):
    'Shareholding Package Lines'
    __name__ = 'company.shareholding.lines'
    package = fields.Many2One('company.shareholding.package', 'Package',
        required=True, select=True)
    type = fields.Many2One('company.shareholding_type', 'Type',
        required=True, select=True)
    shareholding_quantity = fields.Numeric('Shareholding Quantity',
        required=True, digits=(2, 0))
