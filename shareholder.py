# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date
from trytond.model import ModelView, ModelSQL, fields, Workflow
from trytond.transaction import Transaction
from trytond.pyson import Eval
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.report import Report

__all__ = ['CompanyShareholder', 'CompanyShareholdings', 'ShareholdingsEvents']

STATES = [
    ('draft', 'Draft'),
    ('active', 'Active'),
    ('finished', 'Finished')
]

STATES_VIEW = {
    'readonly': Eval('state') != 'draft',
}


def set_number(obj, sequence):
    pool = Pool()
    Sequence = pool.get('ir.sequence')
    Config = pool.get('company.shareholding.configuration')
    config = Config(1)
    if not config:
        return
    sequence = getattr(config, sequence)
    if not sequence:
        obj.raise_user_error('message', 'Not exist \
                             ' + sequence.__str__() + ' in configuration')
    code = Sequence.get_id(sequence.id)
    obj.write([obj], {'code': code})


class CompanyShareholder(ModelSQL, ModelView):
    'Company Shareholder'
    __name__ = 'company.shareholder'
    active = fields.Boolean('Active')
    code = fields.Char('code', readonly=True)
    party = fields.Many2One('party.party', 'Party', required=True)
    notes = fields.Text('Notes')
    shareholdings = fields.One2Many('company.shareholdings', 'shareholder',
                                    'Shareholdings')

    @classmethod
    def __setup__(cls):
        super(CompanyShareholder, cls).__setup__()
        cls._buttons.update({
            'wizard_create_shareholding_package': {},
        })

    @classmethod
    @ModelView.button_action('company_shareholding.wizard_create_shareholding_package')
    def wizard_create_shareholding_package(cls, records):
        pass

    @staticmethod
    def default_active():
        return True

    # @staticmethod
    # def default_shareholdings():
    #     if Transaction().user == 0:
    #         return []
    #     return [{}]

    @classmethod
    def create(cls, records):
        records = super(CompanyShareholder, cls).create(records)
        for rec in records:
            if not rec.code:
                set_number(rec, 'shareholder_sequence')
        return records

    def get_rec_name(self, name):
        party = self.party.rec_name or ''
        self._rec_name = party
        return (self._rec_name)


class CompanyShareholdings(Workflow, ModelSQL, ModelView):
    'Company Shareholdings'
    __name__ = 'company.shareholdings'
    shareholder = fields.Many2One('company.shareholder', 'Shareholder',
                               required=True, select=True, states=STATES_VIEW)
    type = fields.Many2One('company.shareholding_type', 'Type',
        required=True, select=True, states=STATES_VIEW)
    shareholding_quantity = fields.Numeric('Shareholding Quantity', required=True,
                                          digits=(2, 0), states=STATES_VIEW)
    possessor = fields.Many2One('party.party', 'Possessor')
    code = fields.Char('Shareholding Code')
    company = fields.Many2One('company.company', 'Company', required=True,
                              states=STATES_VIEW)
    state = fields.Selection(STATES, 'State', required=True, readonly=True)
    events = fields.One2Many('company.shareholding.events', 'shareholding',
                             'Events')
    description = fields.Char('Description')

    @classmethod
    def __setup__(cls):
        super(CompanyShareholdings, cls).__setup__()
        cls._error_messages.update({
                'message': ('Error %s'),
                })
        cls._transitions |= set((
                ('draft', 'active'),
                ('active', 'draft'),
                ('active', 'finished'),
                ('finished', 'active'),
                ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state') != 'active',
                },
            'active': {
                'invisible': Eval('state') == 'active',
                },
            'finished': {
                'invisible': Eval('state') != 'active',
                },
        })

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('active')
    def active(cls, records):
        for rec in records:
            if not rec.code:
                set_number(rec, 'shareholding_sequence')

    @classmethod
    @ModelView.button
    @Workflow.transition('finished')
    def finished(cls, records):
        pass

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'


class ShareholdingsEvents(ModelSQL, ModelView):
    'Shareholdings Events'
    __name__ = 'company.shareholding.events'
    shareholding = fields.Many2One('company.shareholdings', 'Shareholding',
        required=True, select=True)
    date_event = fields.Date('Date Event', required=True)
    type = fields.Many2One('company.shareholding.event_type', 'Type',
        required=True)
    description = fields.Char('Description', required=True)


class CompanyShareholdingsReport(Report):
    __name__ = 'company.shareholdings.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(CompanyShareholdingsReport, cls).get_context(records, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        report_context['user'] = user
        date_ = date.today()
        report_context['today'] = date_
        return report_context


class CreatePackageStart(ModelView):
    'Create Package Start'
    __name__ = 'company.shareholding.create_package.start'
    package = fields.Many2One('company.shareholding.package', 'Package',
        required=True)
    company = fields.Many2One('company.company', 'Company', required=True)


class CreatePackage(Wizard):
    'Create Package'
    __name__ = 'company.shareholding.create_package'
    start = StateView('company.shareholding.create_package.start',
        'company_shareholding.create_shareholding_package_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        pool = Pool()
        Shareholding = pool.get('company.shareholdings')
        Shareholder = pool.get('company.shareholder')
        package = self.start.package
        company = self.start.company.id
        id_ = Transaction().context['active_id']

        if not id_:
            return 'end'

        shareholder, = Shareholder.browse([id_])
        for sh in package.lines:
            new_obj = {
                'shareholder': id_,
                'type': sh.type.id,
                'company': company,
                'shareholding_quantity': sh.shareholding_quantity,
                'state': 'draft'
            }
            sh_created = Shareholding.create([new_obj])
            Shareholding.active(sh_created)

        return 'end'
